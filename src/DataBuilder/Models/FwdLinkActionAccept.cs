﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBuilder.Models;
public class FwdLinkActionAccept
{
    public string policyno { get; set; }
    public string actioncode { get; set; }
    public string senddate { get; set; }
}
