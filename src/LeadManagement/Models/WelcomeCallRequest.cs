﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LeadManagement.Models;

public class WelcomeCallRequest : BaseModel
{
    public int Id { get; set; }

    [Required]
    public string PolicyNumber { get; set; } = "";

    [Required]
    public string FirstIssueDate { get; set; } = "";
}
