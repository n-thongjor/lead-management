﻿using LeadManagement.Config;
using LeadManagement.Dtos.Salesforce;
using Newtonsoft.Json;
using static LeadManagement.Config.ServicesConfig;

namespace LeadManagement.Services;
public class SalesforceService : ISalesforceService
{
    private readonly Serilog.ILogger _logger;
    private readonly ServicesConfig _servicesConfig;
    private readonly HttpClient _client;

    public SalesforceService(Serilog.ILogger logger, ServicesConfig servicesConfig, HttpClient client)
    {
        _logger = logger;
        _servicesConfig = servicesConfig;
        _client = client;
    }

    public async Task<LoginResponseDto> Login()
    {
        SalesforceServiceConfig salesforce = _servicesConfig.Salesforce;
        var payload = new Dictionary<string, string>
        {
            ["grant_type"] = "password",
            ["client_id"] = salesforce.ClientId,
            ["client_secret"] = salesforce.ClientSecret,
            ["username"] = salesforce.Username,
            ["password"] = salesforce.Password,
        };
        var req = new HttpRequestMessage(HttpMethod.Post, salesforce.LoginUrl) { Content = new FormUrlEncodedContent(payload) };
        HttpResponseMessage res = await _client.SendAsync(req);
        _logger.Information("Login to {Url} by using {@Credentials}", salesforce.LoginUrl, payload);
        string content = await res.Content.ReadAsStringAsync();
        var loginResponse = JsonConvert.DeserializeObject<LoginResponseDto>(content);
        _logger.Information("Receive login result {@Response}", loginResponse);

        if(loginResponse == null)
        {
            throw new Exception("Login response has no content.");
        }

        return loginResponse;
    }
}
