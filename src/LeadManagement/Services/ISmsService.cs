﻿using LeadManagement.Dtos.Ccm;
using LeadManagement.Dtos.Sms;
using System.Net;

namespace LeadManagement.Services;
public interface ISmsService
{
    public Task<string> EnqueueSms(ResendSmsFormDto form);
}
