# Welcome Call

![Lead Management](./images/LeadManagement.png)

## Welcome Call To LM

> Endpoint: /api/v1/WelcomeCall
>
> HTTP Method: POST
>
> Flow: 8 and 9

Input:

```json
{
  "policyNumber": "1234",
  "firstIssueDate": "2022-10-10"
}
```

CURL:

```
curl -X 'POST' \
  'https://localhost:7248/api/v1/WelcomeCall' \
  -H 'accept: text/plain' \
  -H 'Content-Type: application/json' \
  -d '{
  "policyNumber": "123",
  "firstIssueDate": "2022-10-10"
}'
```

Success Response:
HTTP Status: 201 Created

```json
{
  "id": "a1G1m000001N7fqEAC",
  "status": "success",
  "message": ""
}
```

Error Response:
HTTP Status: 400 Bad Request, 500 Internal Server Error

```json
{
  "id": "",
  "status": "fail",
  "message": "Xxxxx"
}
```