﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBuilder.Queries;
internal class Ods
{
    public static string Query1ODS(string firstIssueDate) => $@"
    SELECT  
    PolicyNo 	Policy_Number__c,
    InsuredTitleName	Salutation__c,
    InsuredFirstName	Insured_Name__c,
    InsuredLastName	Insured_SurName__c,
    AgentNo	Agent__c,
    AgentFirstName	Agent_Name__c,
    AgentLastName	Agent_SurName__c,
    AgentMobileNo	Agent_Mobile__c,
    AgentBranchCode	Branch_Code__c,
    ''	Branch_Name__c,
    DespatchAddrLine1	Despatch_Address1__c,
    DespatchAddrLine2	Despatch_Address2__c,
    DespatchAddrLine3	Despatch_Address3__c,
    DespatchAddrLine4	Despatch_Address4__c,
    DespatchAddrLine5	Despatch_Address5__c,
    DespatchProvince	Despatch_City__c,
    DespatchPostcode	Despatch_ZipCode__c,
    InsuredMobileNo	Insured_Mobile__c,
    InsuredHomePhone	Insured_HomePhone__c,
    InsuredEMailAddress	Insured_Email__c,
    OwnerFirstName	Owner_Name__c,
    OwnerLastName	Owner_SurName__c,
    OwnerEMailAddress	Owner_Email__c,
    OwnerMobileNo	Owner_Mobile__c,
    case when EffectiveDate is not null then substring(convert(varchar,EffectiveDate,127),1,10) else null end	Effective_Date__c,
    case when ProposalReceivedDate is not null then substring(convert(varchar,ProposalReceivedDate,127),1,10) else null end 	Settle_Date__c,
    PlanCode	Plan_Code__c,
    PlanDescription	Plan_Name__c,
    ''	Plan_Name_Thai__c,
    CoveragePeriod	Mature_Period__c,
    PremiumPaymentPeriod	Paid_Period__c,
    BillingFrequency	Payment_Mode__c,
    ''	SA_ULB__c,
    ''	Premium_ULB__c,
    ''	SA_ULI__c,
    ''	Premium_ULI__c,
    ''	Topup__c,
    ModalPremium	Total_Premium__c,
    InternalFundCode	Fund_Sec_Code__c,
    ''	Fund_Match__c,
    ''	Fund_Mismatch__c,
    ''	Fund_Unmap__c,
    PayorFirstName	Payer_Name__c,
    PayorLastName	Payer_SurName__c,
    ''	Rider_Plan_1__c,
    ''	Coverage_Sum_Assured_1__c,
    ''	Rider_Modal_Premium_1__c,
    SourceofBusiness	Channel__c,
    ContractType	Contract_Type__c,
    case when IssueDate is not null then substring(convert(varchar,IssueDate,127),1,10) else null end  Policy_Iss_Date__c,
    ''	Diff_Issue__c,
    ''	Customer_Segment__c,
    ''	Segment_Effective_Date__c,
    ''	Vulnerable__c,
    ''	Vulnerable_Type_1__c,
    case when ProposalDate is not null then substring(convert(varchar,ProposalDate,127),1,10) else null end Sign_Date__c,
    case when SystemName in ('IL') and (ContractType like 'UL%' or ContractType like 'BU%') then 'UL' 
         when SystemName in ('LA') and (ContractType like 'UL%' or ContractType like 'BU%') then 'UL' 
	     else 'IL' end	Product_Type__c,
    ''	Seller_Segment__c,
    Memo	Memo_Code__c,
    BillingChannel	Payment_Method__c,
    ICPPaymentMethod	Cashback_Method__c,
    ''	Product_Category__c,
    SystemName	Source__c,
    PolicyDeliveryMode	Delivery_Method__c,
    ''	Risk_Excess_ACCP__c,
    ''	Sub_Channel__c,
    ''	Staff_ID__c,
    ''	Staff_Non_S__c,

    InsuredIDNo	ID_Number__c,
    RiskProfileScore	Risk_Profile_Score__c,
    ICPOption	ICP_Option__c,
    STFundShortDesc	ST_Fund_Short_Desc__c,
    InternalPlanCode	Internal_Plan_Code__c,
    case when FirstIssueDate is not null then substring(convert(varchar,FirstIssueDate,127),1,10) else null end 	First_Issue_Date__c,
    case BillingFrequency WHEN '00' THEN 'Single' WHEN '01' THEN 'Annual' WHEN '02' THEN 'Half-Yearly' WHEN '04' THEN 'Quarterly' WHEN '12' THEN 'Monthly' WHEN 'DY' THEN 'Daily' WHEN 'WK' THEN 'Weekly' else BillingFrequency end AS Payment_Mode_Desc__c,
    case BillingChannel WHEN 'C' THEN 'Cash' WHEN 'D' THEN 'Direct Debit' WHEN 'G' THEN 'Group' WHEN 'N' THEN 'No Billing' WHEN 'R' THEN 'CreditCard' else BillingChannel end AS Payment_Method_Desc__c,
    case PolicyDeliveryMode WHEN 'AGNT' THEN 'Sent to Agent' WHEN 'APKB' THEN 'ตัวแทนรับที่สาขา' WHEN 'APKH' THEN 'ตัวแทนรับที่ สนญ.' WHEN 'BANK' THEN 'Send to Bank' WHEN 'CLNT' THEN 'Sent Client by registered mail' WHEN 'CPKB' THEN 'ลูกค้ารับที่ Bank' WHEN 'CPKH' THEN 'ลูกค้ารับที่สนญ.' WHEN 'CPKS' THEN 'ลูกค้ารับที่สาขา' WHEN 'CSCB' THEN 'ติดต่อสาขาธนาคาร' WHEN 'CSMC' THEN 'ติดต่อ SMC' WHEN 'DTCL' THEN 'ส่งที่อยู่ลูกค้า' WHEN 'EMAL' THEN 'E-Mail' WHEN 'LTEM' THEN 'Letter + E-Mail' WHEN 'SPKH' THEN 'Specialist รับที่ สนญ.' else PolicyDeliveryMode end AS Delivery_Method_Desc__c,
    InsuredAge Insured_Age__c
    FROM fn_R_WelcomeCallPolicy
    ('{firstIssueDate}', '{firstIssueDate}', '3H2,3G9')
	order by SystemName,
	case when(ContractType like 'UL%' or ContractType like 'BU%') then 1
	when PolicyNo like 'C99%' then 2
	when AgentNo ='E00033' and PlanCode in ('E59A') then 3
	when AgentNo ='E00033' then 4
	else 99 end";

    public static string Query2ODS() => $@"
    select PolicyNo,
    sum(case when RiderNumber = '00' then SumAssured else 0 end) SumAssuredBase,
    sum(case when RiderNumber = '00' and CoverageNumber = '01' then InstallmentPremium+SinglePremium else 0 end) PremiumBase, 
    sum(case when RiderNumber!='00' then SumAssured else 0 end) SumAssuredRider,
    sum(case when RiderNumber!='00' then InstallmentPremium+SinglePremium else 0 end) PremiumRider,
    sum(case when RiderNumber = '00' and CoverageNumber>'01' then InstallmentPremium+SinglePremium else 0 end) PremiumTopUp
      from fn_R_WelcomeCallCoverage(DATEADD(day,-31, getdate()), getdate()) 
    group by PolicyNo
    ";



}
