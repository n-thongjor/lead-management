﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBuilder.Queries;
internal class SpeedyLoan
{
    public static string QuerySpeedyLoan() => @"
    SELECT InsuredCitizenNo
    ,AppLoanNumber
    ,AccountNumber
	,substring(ChannelPackageCode,CHARINDEX(' ',ChannelPackageCode)+1,len(ChannelPackageCode)-CHARINDEX(' ',ChannelPackageCode)) ChannelPackageCode
    FROM Insured i left outer join Loan l on i.ApplicationTransactionId=l.ApplicationTransactionId
    left outer join ApplicationTransaction a on i.ApplicationTransactionId=a.ApplicationTransactionId
    where InsuredCitizenNo in ";
}
