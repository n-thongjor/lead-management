﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using System.ComponentModel;

namespace LeadManagement.Models;

public class ResendSms : BaseModel
{
    public int Id { get; set; }

    [Required]
    public string SourceOfSubmission { get; set; } = "";

    [Required]
    public string ReportType { get; set; } = "";

    [Required]
    public string ReportKey { get; set; } = "";

    [Required]
    public string SendReportKey { get; set; } = "";

    [Required]
    public string SubmissionDate { get; set; } = "";

    [Required]
    public string PolicyNo { get; set; } = "";

    [Required]
    public string RequestPayload { get; set; } = "";

    [DefaultValue("System")]
    public string CreatedBy { get; set; } = "System";

    [DefaultValue("System")]
    public string UpdatedBy { get; set; } = "System";

    [DefaultValue("Pending")]
    public string Status { get; set; } = "Pending";

    public string StatusMessage { get; set; } = "";
}
