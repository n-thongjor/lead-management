﻿namespace LeadManagement.Dtos.Ccm;

public class CreateSmsResponseDto
{
    public class DataField
    {
        public string? RptId { get; set; }
    }

    public string Status { get; set; } = "FAIL";
    public string Message { get; set; } = "FAIL";
    public DataField? Data { get; set; }
}

