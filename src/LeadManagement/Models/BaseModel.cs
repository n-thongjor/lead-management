﻿namespace LeadManagement.Models;

public abstract class BaseModel
{
    public DateTimeOffset CreatedOn { get; set; }
    public DateTimeOffset UpdatedOn { get; set; }
}
