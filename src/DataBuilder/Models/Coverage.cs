﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBuilder.Models;
public class Coverage
{
    public string PolicyNo { get; set; }
    public string SumAssuredBase { get; set; }
    public string PremiumBase { get; set; }
    public string SumAssuredRider { get; set; }
    public string PremiumRider { get; set; }
    public string PremiumTopUp { get; set; }

}
