using DataBuilder;
using LeadManagement.Config;
using LeadManagement.Context;
using LeadManagement.Extensions;
using LeadManagement.Services;
using DataBuilder.Config;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Serilog;


try
{
    var builder = WebApplication.CreateBuilder(args);

    // Add services to the container.

    builder.Services.AddControllers();
    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();
    builder.Services.AddApiVersioning(o =>
    {
        o.AssumeDefaultVersionWhenUnspecified = true;
        o.DefaultApiVersion = new Microsoft.AspNetCore.Mvc.ApiVersion(1, 0);
        o.ReportApiVersions = true;
        o.ApiVersionReader = ApiVersionReader.Combine(
            new QueryStringApiVersionReader("api-version"),
            new HeaderApiVersionReader("X-Version"),
            new MediaTypeApiVersionReader("ver"));
    });

    var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
    var configuration = new ConfigurationBuilder()
        .AddJsonFile("appsettings.json", optional: false)
        .AddJsonFile($"appsettings.{environment}.json", optional: true)
        .Build();
    var logger = new LoggerConfiguration()
        .ReadFrom.Configuration(configuration)
        .CreateLogger();
    builder.Host.UseSerilog(logger);

    builder.Services.AddSingleton(configuration.GetSection("UnderlyingServices").Get<ServicesConfig>());
    builder.Services.AddSingleton(configuration.GetSection("DataBuilder").Get<DataBuilderConfig>());


    #region Services Injection
    builder.Services.AddScoped<ISalesforceService, SalesforceService>();
    builder.Services.AddScoped<HttpClient, HttpClient>();
    builder.Services.AddScoped<IUcrmService, UcrmService>();
    builder.Services.AddScoped<IWelcomeCallService, WelcomeCallService>();
    builder.Services.AddScoped<ICcmService, CcmService>();
    builder.Services.AddScoped<ISmsService, SmsService>();
    builder.Services.AddScoped<IBuilder, Builder>();
    #endregion

    builder.Services.AddDbContext<DataContext>(options => options.UseSqlServer(configuration.GetConnectionString("LeadManagementContext")));

    var app = builder.Build();
    app.UseSerilogRequestLogging();
    app.ConfigureExceptionHandler(logger);

    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    app.UseHttpsRedirection();

    app.UseAuthorization();

    app.MapControllers();

    app.Run();
}
catch (Exception ex)
{
    Console.WriteLine(ex);
    Log.Fatal(ex, "Unhandled exception");
}
finally
{
    Log.Information("Shut down complete");
    Log.CloseAndFlush();
}
