﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBuilder.Models;
public class SalesSegment
{
    public string staffid { get; set; }
    public string scbstaffid { get; set; }
    public string segment { get; set; }
    public string segmentGroup { get; set; }
    public string segmentAPE { get; set; }

}
