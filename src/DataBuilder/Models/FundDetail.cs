﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBuilder.Models;
public class FundDetail
{
    public string FundCode { get; set; }
    public string FundENName { get; set; }
    public string FundTHName { get; set; }
    public string FundCoreCode { get; set; }
    public string Channel { get; set; }
    public string ForeignExchangeRateRisk { get; set; }
    public string ExpiryDate { get; set; }
    public int FundRiskLevel { get; set; }
    public string InvestmentStrategy { get; set; }
    public string AMCCode { get; set; }
    public string AMCName { get; set; }
    public string AssetClassCode { get; set; }
    public string AssetClassENName { get; set; }
    public string DescriptionTH { get; set; }
    public string AssetClassTHName { get; set; }
    public string AssetClassDescription { get; set; }

}
