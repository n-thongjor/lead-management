﻿using LeadManagement.Dtos.Ccm;
using LeadManagement.Dtos.Sms;
using LeadManagement.Dtos.Ucrm;
using LeadManagement.Dtos.WelcomeCall;
using LeadManagement.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace LeadManagement.Controllers;
[Route("api/v{version:apiVersion}/[controller]")]
[ApiController]
[ApiVersion("1.0")]
public class SmsController : ControllerBase
{
    private readonly Serilog.ILogger _logger;
    private readonly ISmsService _smsService;
    private readonly ICcmService _ccmService;

    public SmsController(Serilog.ILogger logger, ISmsService smsService, ICcmService ccmService)
    {
        _logger = logger;
        _smsService = smsService;
        _ccmService = ccmService;
    }

    [HttpPost]
    public async Task<ActionResult> ResendSms()
    {
        _logger.Debug("ResendSms is called");

        try
        {
            await _ccmService.ResendSms();

            return CreatedAtAction(nameof(ResendSms), new
            {
                Status = "Success",
                Message = "Success"
            });
        }
        catch (Exception ex)
        {
            return BadRequest(new
            {
                Status = "Success",
                Message = ex.Message
            });
        }
    }

    [HttpPost("queues")]
    public async Task<ActionResult> EnqueueSms([FromBody] ResendSmsFormDto form)
    {
        _logger.Debug("EnqueueSms is called");


        try
        {
            string rptId = await _smsService.EnqueueSms(form);

            return CreatedAtAction(nameof(ResendSms), new
            {
                Status = "Success",
                Message = "Success",
                Data = new
                {
                    RptId = rptId
                }
            });
        }
        catch (Exception ex)
        {
            return BadRequest(new
            {
                Status = "Fail",
                Message = ex.Message ?? "Something went wrong"
            });
        }
    }
}
