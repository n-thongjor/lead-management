﻿using RestSharp;
using Newtonsoft.Json;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Net;
using DataBuilder.Models;
using DataBuilder.Config;
using Newtonsoft.Json.Linq;
using DataBuilder.Queries;
using MySqlX.XDevAPI.Common;

namespace DataBuilder;

public record BuildResponse(string? JsonString, Exception? Error, string? DuplicateId);


public class Builder : IBuilder
{

    public readonly DataBuilderConfig _config;
    private readonly Serilog.ILogger _logger;

    public Builder(DataBuilderConfig config, Serilog.ILogger logger)
    {
        _config = config;
        _logger = logger;
    }

    public DataTable ExecuteQuerySqlServer(string query, string Server)
    {

        string dbConnectionString = _config.ConnectionStrings.GetType().GetProperty(Server).GetValue(_config.ConnectionStrings, null).ToString();


        using (SqlConnection conn = new SqlConnection(Base64Decode(dbConnectionString)))
        {
            using (SqlCommand command = new SqlCommand(query, conn))
            {
                try
                {

                    conn.Open();

                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    int timeout = _config.DBConnectionTimeOut;
                    da.SelectCommand.CommandTimeout = timeout;
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Table1");
                    return ds.Tables[0];
                }
                catch (SqlException ex)
                {
                    _logger.Error(ex.Message);
                    return null;
                }
                finally
                {
                    conn.Close();
                }
            }
        }

    }

    public BuildResponse Build(string firstIssueDate, string policyNo)
    {
        CultureInfo.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
        _logger.Information("===============================================================================================");
        _logger.Information("Program DataPrepWelcomeCallObject.start");

        _logger.Information("========");
        _logger.Information("Program DataPrepWelcomeCallObject.SalesForce.login start");
        List<string> EwcListPol = new List<String>();
        try
        {
            string baseAddress = _config.SFbaseAddress;
            string client_id = _config.SFclientId;
            string client_secret = _config.SFclientSecret;
            string username = _config.SFusername;
            string password = _config.SFpassword;
            Token x = GetAuthorizeToken(baseAddress, client_id, client_secret, username, password);

            _logger.Information("Program DataPrepWelcomeCallObject.SalesForce.login End");
            _logger.Information("========");
            _logger.Information("Program DataPrepWelcomeCallObject.SalesForce.GetExistWelcomecall start");

            string QueryAPIURL = _config.SFQueryAPIURL;
            string QuerySOQLExistsWelcomeCall = Sf.SFQuerySOQLExistsWelcomeCall();
            string tokenString = x.access_token;
            string instance_url = x.instance_url;
            int runningPage = 0;


            System.Threading.Thread.Sleep(_config.WaitingTimePerRequest);
            SFQueryResult[] EwcList = new SFQueryResult[100];
            EwcList[runningPage] = GetExistWelcomeCall(tokenString, instance_url, QueryAPIURL, QuerySOQLExistsWelcomeCall);

            int[] totalrecordsEwcList = new int[100];
            totalrecordsEwcList[runningPage] = EwcList[runningPage].records.Count();

            if (totalrecordsEwcList[runningPage] > 0)
            {
                for (int i = 0; i < totalrecordsEwcList[runningPage]; i++)
                {

                    EwcListPol.Add(EwcList[runningPage].records[i].Policy_Number__c);
                    EwcListPol.Add(EwcList[runningPage].records[i].Id);
                }
            }
            instance_url = x.instance_url + EwcList[runningPage].nextRecordsUrl;
            QueryAPIURL = "";
            QuerySOQLExistsWelcomeCall = "";
            while (!String.IsNullOrEmpty(EwcList[runningPage].nextRecordsUrl) && totalrecordsEwcList[runningPage] > 0)
            {
                runningPage++;
                EwcList[runningPage] = GetExistWelcomeCall(tokenString, instance_url, QueryAPIURL, QuerySOQLExistsWelcomeCall);
                totalrecordsEwcList[runningPage] = EwcList[runningPage].records.Count();
                System.Threading.Thread.Sleep(_config.WaitingTimePerRequest);

                if (totalrecordsEwcList[runningPage] > 0)
                {
                    for (int i = 0; i < totalrecordsEwcList[runningPage]; i++)
                    {

                        EwcListPol.Add(EwcList[runningPage].records[i].Policy_Number__c);
                    }
                }
                instance_url = x.instance_url + EwcList[runningPage].nextRecordsUrl; ;
            }
            _logger.Information("========" + EwcListPol.Count());
        }
        catch (Exception ex)
        {
            _logger.Error("Program DataPrepWelcomeCallObject.SalesForce.Login/GetExistWelcomecall Error Exception:" + ex.ToString());
            EwcListPol.Clear();
            return new BuildResponse
           (
               JsonString: null,
               Error: ex,
               DuplicateId: null
           );
        }


        _logger.Information("Program DataPrepWelcomeCallObject.SalesForce.GetExistWelcomecall End");
        _logger.Information("========");

        _logger.Information("Program DataPrepWelcomeCallObject.Sqlserver.ODS start");
        // ODS #1 is parameter to filter this step
        DataTable dtBase;
        DataRow dtRow;
        try
        {
            string query1 = Ods.Query1ODS(firstIssueDate);
            dtBase = ExecuteQuerySqlServer(query1, "ODS");
            if (dtBase.Rows.Count == 0) return null;
            dtBase.Columns.Add("Channel_Long_Desc__c");
            dtBase.Columns.Add("Channel_Short_Desc__c");
            dtBase.Columns.Add("Contract_Type_Long_Desc__c");
            dtBase.Columns.Add("App_Loan_Number__c");
            dtBase.Columns.Add("Loan_Account_Number__c");
            dtBase.Columns.Add("Welcome_Call_SMS__c");
            dtRow = dtBase.AsEnumerable().Where(row => row.Field<string>("Policy_Number__c") == policyNo).First();
            if (dtRow == null) return null;
        }
        catch (Exception ex)
        {
            _logger.Error("Program DataPrepWelcomeCallObject.Sqlserver.ODS Error Exception:" + ex.ToString());
            return new BuildResponse
            (
                JsonString: null,
                Error: ex,
                DuplicateId: null
            );
        }

        _logger.Information("Program DataPrepWelcomeCallObject.Sqlserver.ODS End");

        _logger.Information("========");
        _logger.Information("Program DataPrepWelcomeCallObject.SalesForce.CheckDuplicate start");





        _logger.Information("Program DataPrepWelcomeCallObject.SalesForce.CheckDuplicate End");
        _logger.Information("========");
        _logger.Information("Program DataPrepWelcomeCallObject.Sqlserver.DWH start");
        // DWH #2 is parameter to filter this step
        try
        {
            _logger.Information("Program DataPrepWelcomeCallObject.Sqlserver.Query2ODS start");
            string Query2ODS = Ods.Query2ODS();
            DataTable dtMaster2ODS = ExecuteQuerySqlServer(Query2ODS, "ODS");
            _logger.Information("Program DataPrepWelcomeCallObject.Sqlserver.Query2ODS End");
            List<Coverage> CoverageList = new List<Coverage>();
            foreach (DataRow dtMaster2Row in dtMaster2ODS.Rows)
            {
                Coverage wcc2 = new Coverage();
                wcc2.PolicyNo = dtMaster2Row["PolicyNo"].ToString();
                wcc2.SumAssuredBase = dtMaster2Row["SumAssuredBase"].ToString();
                wcc2.PremiumBase = dtMaster2Row["PremiumBase"].ToString();
                wcc2.SumAssuredRider = dtMaster2Row["SumAssuredRider"].ToString();
                wcc2.PremiumRider = dtMaster2Row["PremiumRider"].ToString();
                wcc2.PremiumTopUp = dtMaster2Row["PremiumTopUp"].ToString();
                CoverageList.Add(wcc2);
            }
            _logger.Information("Program DataPrepWelcomeCallObject.Sqlserver.QueryDWHSourceOfBiz start");
            string query2 = Dwh.QueryDWHSourceOfBiz();
            DataTable dtMaster2 = ExecuteQuerySqlServer(query2, "DWH");
            _logger.Information("Program DataPrepWelcomeCallObject.Sqlserver.QueryDWHSourceOfBiz End");
            List<SourceOfBiz> sourceOfBizList = new List<SourceOfBiz>();
            foreach (DataRow dtMaster2Row in dtMaster2.Rows)
            {
                SourceOfBiz sBiz2 = new SourceOfBiz();
                sBiz2.SHORTDESC = dtMaster2Row["SHORTDESC"].ToString();
                sBiz2.LONGDESC = dtMaster2Row["LONGDESC"].ToString();
                sBiz2.DESCITEM = dtMaster2Row["DESCITEM"].ToString();
                sBiz2.CORE = dtMaster2Row["CORE"].ToString();
                sourceOfBizList.Add(sBiz2);
            }
            _logger.Information("Program DataPrepWelcomeCallObject.Sqlserver.QueryDWHContractType start");
            string query3 = Dwh.QueryDWHContractType();
            DataTable dtMaster3 = ExecuteQuerySqlServer(query3, "DWH");
            _logger.Information("Program DataPrepWelcomeCallObject.Sqlserver.QueryDWHContractType End");

            List<ContractType> ContractTypeList = new List<ContractType>();
            foreach (DataRow dtMaster3Row in dtMaster3.Rows)
            {
                ContractType cType2 = new ContractType();
                cType2.SHORTDESC = dtMaster3Row["SHORTDESC"].ToString();
                cType2.LONGDESC = dtMaster3Row["LONGDESC"].ToString();
                cType2.DESCITEM = dtMaster3Row["DESCITEM"].ToString();
                cType2.CORE = dtMaster3Row["CORE"].ToString();
                ContractTypeList.Add(cType2);
            }

            List<Vulnerable> VulnerableList = new List<Vulnerable>();
            string query4 = "DECLARE @PolicyList as TableType;";
            if (dtRow["Contract_Type__c"].ToString().Substring(0, 2) == "UL" || dtRow["Contract_Type__c"].ToString().Substring(0, 2) == "BU")
            {
                query4 = query4 + "insert into  @PolicyList values('" + dtRow["Policy_Number__c"].ToString() + "');";
            }
            _logger.Information("Program DataPrepWelcomeCallObject.Sqlserver.QueryDWHVulnerable start");
            query4 = query4 + Dwh.QueryDWHVulnerable();
            DataTable dtVulnerable = ExecuteQuerySqlServer(query4, "DWH");
            _logger.Information("Program DataPrepWelcomeCallObject.Sqlserver.QueryDWHVulnerable End");

            foreach (DataRow dtVulRow in dtVulnerable.Rows)
            {
                Vulnerable vflag2 = new Vulnerable();
                vflag2.PolicyNo = dtVulRow["PolicyNo"].ToString();
                vflag2.VulnerableType = dtVulRow["VulnerableType"].ToString();
                vflag2.VulnerableFlag = dtVulRow["VulnerableFlag"].ToString();
                VulnerableList.Add(vflag2);
            }

            List<CustomerTier> CustomerTierList = new List<CustomerTier>();

            _logger.Information("Program DataPrepWelcomeCallObject.Sqlserver.CustomerTier start");
            string query5 = "DECLARE @CustomerIDList as TableType;";
            query5 = query5 + "insert into  @CustomerIDList values('" + dtRow["ID_Number__c"].ToString() + "');";
            query5 = query5 + Dwh.QueryDWHCustomerTier();
            DataTable dtCustomerTier = ExecuteQuerySqlServer(query4, "DWH");
            _logger.Information("Program DataPrepWelcomeCallObject.Sqlserver.CustomerTier End");

            foreach (DataRow dtRowCust in dtCustomerTier.Rows)
            {
                CustomerTier cflag2 = new CustomerTier();

                cflag2.CardId = dtRowCust["CardId"].ToString();
                cflag2.CustomerTierLevel = dtRowCust["CustomerTier"].ToString();
                cflag2.CustomerTierDescription = dtRowCust["CustomerTierDescription"].ToString();
                cflag2.TierEffectiveDate = dtRowCust["TierEffectiveDate"].ToString();
                CustomerTierList.Add(cflag2);
            }

            // map master table to base table

            // find Coverage tabel


            Coverage wcc = CoverageList.Find(s => s.PolicyNo == dtRow["Policy_Number__c"].ToString());
            if (wcc != null)
            {

                if (wcc.SumAssuredBase == "0.00" && wcc.SumAssuredRider == "0.00")
                {
                    dtRow["Coverage_Sum_Assured_1__c"] = wcc.PremiumBase;
                    dtRow["SA_ULB__c"] = wcc.PremiumBase;

                }
                else
                {
                    dtRow["Coverage_Sum_Assured_1__c"] = wcc.SumAssuredBase;
                    dtRow["SA_ULB__c"] = wcc.SumAssuredBase;
                }
                dtRow["Premium_ULB__c"] = wcc.PremiumBase;
                dtRow["Topup__c"] = wcc.PremiumTopUp;
                dtRow["SA_ULI__c"] = wcc.SumAssuredRider;
                dtRow["Premium_ULI__c"] = wcc.PremiumRider;
            }


            // find sourceOfBiz tabel
            SourceOfBiz sBiz = sourceOfBizList.Find(s => s.DESCITEM == dtRow["Channel__c"].ToString() && s.CORE == dtRow["Source__c"].ToString());
            if (sBiz != null)
            {
                dtRow["Channel_Long_Desc__c"] = sBiz.LONGDESC;
                dtRow["Channel_Short_Desc__c"] = sBiz.SHORTDESC;

            }
            // find ContractType tabel
            ContractType cType = ContractTypeList.Find(s => s.DESCITEM == dtRow["Contract_Type__c"].ToString() && s.CORE == dtRow["Source__c"].ToString());
            if (cType != null)
            {
                dtRow["Contract_Type_Long_Desc__c"] = cType.LONGDESC;
                // dtRow["Contract_Type_Short_Desc__c"] = cType.SHORTDESC;

            }

            // find Vulnerable tabel
            Vulnerable vflag = VulnerableList.Find(s => s.PolicyNo == dtRow["Policy_Number__c"].ToString());
            if (vflag != null)
            {
                dtRow["Vulnerable__c"] = vflag.VulnerableFlag;
                dtRow["Vulnerable_Type_1__c"] = vflag.VulnerableType;
            }

            // find CustomerTeir tabel
            CustomerTier cflag = CustomerTierList.Find(s => s.CardId == dtRow["ID_Number__c"].ToString());
            if (cflag != null)
            {
                dtRow["Customer_Segment__c"] = cflag.CustomerTierDescription;
                dtRow["Segment_Effective_Date__c"] = cflag.TierEffectiveDate;
            }

        }
        catch (Exception ex)
        {
            _logger.Error("Program DataPrepWelcomeCallObject.Sqlserver.DWH Error Exception:" + ex.ToString());
            // return new BuildResponse
            //(
            //    JsonString: null,
            //    Error: ex,
            //    DuplicateId: null
            //);
        }
        finally
        {

        }
        _logger.Information("Program DataPrepWelcomeCallObject.Sqlserver.DWH End");
        _logger.Information("========");

        _logger.Information("========");
        _logger.Information("Program DataPrepWelcomeCallObject.callWSRiskProfileScore start");
        // Risk Profile Score info
        string match = "";
        string mismatch = "";
        if (!String.IsNullOrEmpty(dtRow["Risk_Profile_Score__c"].ToString()))
        {
            RpqAssessment UpdateRpqAssessmentList = CheckRpqAssessment(dtRow["Risk_Profile_Score__c"].ToString());

            dtRow["Risk_Excess_ACCP__c"] = UpdateRpqAssessmentList.InvestorRiskLevel.ToString();

            string AllowedFundRiskLevel = UpdateRpqAssessmentList.AllowedFundRiskLevel.ToString();
            AllowedFundRiskLevel = AllowedFundRiskLevel.Replace("[", "").Replace("]", "");

            System.Threading.Thread.Sleep(_config.WaitingTimePerRequest);
            if (!String.IsNullOrEmpty(dtRow["Fund_Sec_Code__c"].ToString()))
            {
                FundDetail[] UpdateFundDetailList = CheckFundDetail(dtRow["Fund_Sec_Code__c"].ToString());
                System.Threading.Thread.Sleep(_config.WaitingTimePerRequest);
                int totalRowNumber = UpdateFundDetailList.Count();

                for (int i = 0; i < totalRowNumber; i++)
                {
                    if (AllowedFundRiskLevel.Contains(UpdateFundDetailList[i].FundRiskLevel.ToString()))
                    {
                        match = match + "[" + UpdateFundDetailList[i].FundCoreCode.ToString() + "|" + UpdateFundDetailList[i].FundCode.ToString() + "|" + UpdateFundDetailList[i].FundRiskLevel.ToString() + "],";
                        _logger.Information("------PolicyNo: " + dtRow["Policy_Number__c"].ToString() + "|" + dtRow["Risk_Excess_ACCP__c"].ToString() + "[match]" + UpdateFundDetailList[i].FundRiskLevel.ToString() + "|" + UpdateFundDetailList[i].FundCoreCode.ToString() + "|" + UpdateFundDetailList[i].FundCode.ToString() + "------");
                    }
                    else
                    {
                        mismatch = mismatch + "[" + UpdateFundDetailList[i].FundCoreCode.ToString() + "|" + UpdateFundDetailList[i].FundCode.ToString() + "|" + UpdateFundDetailList[i].FundRiskLevel.ToString() + "],";
                        _logger.Information("------PolicyNo: " + dtRow["Policy_Number__c"].ToString() + "|" + dtRow["Risk_Excess_ACCP__c"].ToString() + "[mismatch]" + UpdateFundDetailList[i].FundRiskLevel.ToString() + "|" + UpdateFundDetailList[i].FundCoreCode.ToString() + "|" + UpdateFundDetailList[i].FundCode.ToString() + "------");
                        dtRow["Risk_Excess_ACCP__c"] = "Y";
                    }

                }
                dtRow["Fund_Match__c"] = match;
                dtRow["Fund_Mismatch__c"] = mismatch;

                // _logger.Information("------PolicyNo: " + dtRow["Policy_Number__c"].ToString() + "|" + dtRow["Risk_Excess_ACCP__c"].ToString() + "------");

            }
        }
        _logger.Information("Program DataPrepWelcomeCallObject.callWSRiskProfileScore End");

        _logger.Information("========");

        _logger.Information("Program DataPrepWelcomeCallObject.Sqlserver.SalesStructure start");
        // SalesStructure #2 is parameter to filter this step
        try
        {
            string query5 = Mis.QueryMISSalesStruct();
            DataTable dtMaster5 = ExecuteQuerySqlServer(query5, "MIS");

            List<SalesStruct> SalesStructList = new List<SalesStruct>();
            foreach (DataRow dtMaster5Row in dtMaster5.Rows)
            {
                SalesStruct sStruct = new SalesStruct();

                sStruct.fwdcolumn1 = dtMaster5Row["fwdcolumn1"].ToString();
                sStruct.Column1 = dtMaster5Row["Column1"].ToString();
                sStruct.fwdcolumn2 = dtMaster5Row["fwdcolumn2"].ToString();
                sStruct.Column2 = dtMaster5Row["Column2"].ToString();
                sStruct.Column3 = dtMaster5Row["Column3"].ToString();
                SalesStructList.Add(sStruct);
            }
            dtRow["Welcome_Call_SMS__c"] = "Y";
            // find sourceOfBiz tabel
            if (dtRow["Source__c"].ToString() == "IL")
            {
                SalesStruct sStruct = SalesStructList.Find(s => s.fwdcolumn1 == dtRow["Agent__c"].ToString());
                if (sStruct != null)
                {

                    dtRow["Staff_ID__c"] = sStruct.Column2.Substring(1, sStruct.Column2.Length - 1);
                    dtRow["Staff_Non_S__c"] = sStruct.Column1 + "|" + dtRow["Agent_Name__c"].ToString() + " " + dtRow["Agent_SurName__c"].ToString();

                }
            }
            string query6 = Mis.QueryMISSalesSegment();
            DataTable dtMaster6 = ExecuteQuerySqlServer(query6, "MIS");
            List<SalesSegment> SalesSegmentList = new List<SalesSegment>();
            foreach (DataRow dtMaster6Row in dtMaster6.Rows)
            {
                SalesSegment sSegment = new SalesSegment();

                sSegment.staffid = dtMaster6Row["staffid"].ToString();
                sSegment.scbstaffid = dtMaster6Row["scbstaffid"].ToString();
                sSegment.segment = dtMaster6Row["segment"].ToString();
                sSegment.segmentGroup = dtMaster6Row["segmentGroup"].ToString();
                sSegment.segmentAPE = dtMaster6Row["segmentAPE"].ToString();
                SalesSegmentList.Add(sSegment);
            }
            // find sourceOfBiz tabel
            if (dtRow["Source__c"].ToString() == "IL")
            {
                SalesSegment sSegment = SalesSegmentList.Find(s => s.staffid == dtRow["Agent__c"].ToString());
                if (sSegment != null)
                {

                    dtRow["Seller_Segment__c"] = sSegment.segmentAPE.ToString();

                }
                else
                {

                    dtRow["Seller_Segment__c"] = "Other";
                }
            }

        }
        catch (Exception ex)
        {
            _logger.Error("Program DataPrepWelcomeCallObject.Sqlserver.SalesStructure Error Exception:" + ex.ToString());
            return new BuildResponse
           (
               JsonString: null,
               Error: ex,
               DuplicateId: null
           );
        }


        _logger.Information("Program DataPrepWelcomeCallObject.Sqlserver.SalesStructure End");
        _logger.Information("Program DataPrepWelcomeCallObject.Sqlserver.SpeedyLoan start");
        int cnt = 0;
        String IDSpeedy = "(";
        try
        {
            if (dtRow["Source__c"].ToString() == "IL" && (dtRow["Channel__c"].ToString() == "BT" || dtRow["Channel__c"].ToString() == "TM" || dtRow["Channel__c"].ToString() == "BL") && dtRow["ID_Number__c"].ToString() != "")
            {
                if (IDSpeedy == "(")
                {
                    IDSpeedy = IDSpeedy + "'" + dtRow["ID_Number__c"].ToString() + "'";
                }
                else
                {
                    IDSpeedy = IDSpeedy + ",'" + dtRow["ID_Number__c"].ToString() + "'";
                }
                cnt = cnt + 1;
            }

            IDSpeedy = IDSpeedy + ")";
            if (cnt > 0)
            {
                string query6 = SpeedyLoan.QuerySpeedyLoan() + IDSpeedy;
                DataTable dtMaster6 = ExecuteQuerySqlServer(query6, "Eform");

                List<SpeedyLoanByAccount> SpeedyLoanByAccountList = new List<SpeedyLoanByAccount>();
                foreach (DataRow dtMaster6Row in dtMaster6.Rows)
                {
                    SpeedyLoanByAccount sLoan = new SpeedyLoanByAccount();

                    sLoan.InsuredCitizenNo = dtMaster6Row["InsuredCitizenNo"].ToString();
                    sLoan.AppLoanNumber = dtMaster6Row["AppLoanNumber"].ToString();
                    sLoan.AccountNumber = dtMaster6Row["AccountNumber"].ToString();
                    sLoan.ChannelPackageCode = dtMaster6Row["ChannelPackageCode"].ToString();

                    SpeedyLoanByAccountList.Add(sLoan);
                }
                // find sourceOfBiz tabel
                if (dtRow["Source__c"].ToString() == "IL" && (dtRow["Channel__c"].ToString() == "BT" || dtRow["Channel__c"].ToString() == "TM" || dtRow["Channel__c"].ToString() == "BL"))
                {
                    SpeedyLoanByAccount sLoan = SpeedyLoanByAccountList.Find(s => s.InsuredCitizenNo == dtRow["ID_Number__c"].ToString());
                    if (sLoan != null)
                    {

                        dtRow["App_Loan_Number__c"] = sLoan.AppLoanNumber.ToString();
                        dtRow["Loan_Account_Number__c"] = sLoan.AccountNumber.ToString();

                    }
                }
            }
        }
        catch (Exception ex)
        {
            _logger.Error("Program DataPrepWelcomeCallObject.Sqlserver.SpeedyLoan Error Exception:" + ex.ToString());
            return new BuildResponse
           (
               JsonString: null,
               Error: ex,
               DuplicateId: null
           );
        }

        _logger.Information("Program DataPrepWelcomeCallObject.WriteDataToFile start");
        DataTable dtBaseMustCall;
        DataTable dtBaseCustomerAccept;
        DataTable dtBaseDuplicate;



        dtBaseMustCall = new DataTable();
        dtBaseDuplicate = new DataTable();

        for (int i = 0; i < dtBase.Columns.Count; i++)
        {
            dtBaseMustCall.Columns.Add(dtBase.Columns[i].ColumnName);
            dtBaseDuplicate.Columns.Add(dtBase.Columns[i].ColumnName);
        }


        bool dup = false;
        string dupId = "";
        if (EwcListPol.Count() > 0)
        {
            for (int j = 0; j < EwcListPol.Count(); j++)
                //check duplicate
                if (EwcListPol[j] == dtRow["Policy_Number__c"].ToString())
                {
                    dup = true;
                    dupId = EwcListPol[j + 1];
                    break;
                }
        }

        dtBase.Columns["Source__c"].ColumnName = "Source_System__c";
        dtBase.Columns["ID_Number__c"].ColumnName = "Id_Card_Number__c";
        dtBase.Columns["ICP_Option__c"].ColumnName = "ICP_Options__c";
        dtBase.Columns["Insured_Age__c"].ColumnName = "Age__c";
        dtBase.Columns["Agent__c"].ColumnName = "Agent_Code__c";

        dtRow["Segment_Effective_Date__c"] = "2022-08-22";



        if (!dup)
        {
            dtBaseMustCall.ImportRow(dtRow);
        }
        else
        {
            // dump record to DupplicateLeadTable (Id, policy_no, CreatedOn)
            dtBaseDuplicate.ImportRow(dtRow);
           return new BuildResponse
           (
               JsonString: null,
               Error: null,
               DuplicateId: dupId
           );
        }



        if (dtBaseMustCall.Rows.Count != 0)
        {
            var result = DataTableToJSON(dtBaseMustCall);
            return new BuildResponse
             (
                 JsonString: result,
                 Error: null,
                 DuplicateId: null
             );
        }


        _logger.Information("Program DataPrepWelcomeCallObject.WriteDataToFile End");
        _logger.Information("========");

        _logger.Information("Program DataPrepWelcomeCallObject.End");
        _logger.Information("===============================================================================================");

        return new BuildResponse
        (
            JsonString: null,
            Error: new Exception("dtBaseMustCall not found"),
            DuplicateId: null
        );
    }

    public string DataTableToJSON(DataTable dt)
    {
        var ignoreColumns = new string[]  {
            "Source__c",
            "ID_Number__c",
            "ICP_Option__c",
            "Insured_Age__c",
            "Agent__c"
        };
        string json = new JObject(
            dt.Columns
              .Cast<DataColumn>()
              .Where(c => !ignoreColumns.Contains(c.ColumnName))
              .Select(c => new JProperty(c.ColumnName, JToken.FromObject(dt.Rows[0][c])))
        ).ToString(Formatting.None);

        return json;
    }
    public RpqAssessment CheckRpqAssessment(string Risk_Profile_Score__c)
    {

        try
        {

            _logger.Information("------call CheckRpqAssessment Start------");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var client = new RestClient(_config.ServerFundWS + _config.MethodRpqassessment);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            var request = new RestRequest();
            request.Method = Method.POST;
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.AddParameter("application/json", "{\"InvestorType\": \"Individual\",\"InvestorScore\":" + Risk_Profile_Score__c.ToString() + "}"
                , ParameterType.RequestBody);


            var response = client.Execute(request);
            var content = response.Content; // raw content as string  
            if (response.StatusCode == HttpStatusCode.OK)
            {

                RpqAssessment result = JsonConvert.DeserializeObject<RpqAssessment>(content);
                _logger.Information("------call CheckRpqAssessment End------");
                return result;

            }
            else
            {
                _logger.Error("call CheckRpqAssessment response Status  =>" + response.StatusCode + " : Message : " + JsonConvert.SerializeObject(content));
                return null;
            }

        }
        catch (Exception ex)
        {
            _logger.Error("Error Exception CheckRpqAssessment =>" + ex.ToString());
            return null;
        }

    }

    public FundDetail[] CheckFundDetail(string InternalFundCode)
    {

        try
        {

            _logger.Information("------call CheckFundDetail Start------");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var client = new RestClient(_config.ServerFundWS + _config.MethodFunddetail);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            var request = new RestRequest();
            request.Method = Method.POST;
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.AddParameter("application/json", "{\"FundCoreCode\": \"" + InternalFundCode + "\",\"BackwardHistoryMonths\": 1}"
                //" JsonConvert.SerializeObject(obj)
                , ParameterType.RequestBody);


            var response = client.Execute(request);
            var content = response.Content; // raw content as string  
            if (response.StatusCode == HttpStatusCode.OK)
            {

                FundDetail[] result = JsonConvert.DeserializeObject<FundDetail[]>(content);
                _logger.Information("------call CheckFundDetail End------");
                return result;

            }
            else
            {
                _logger.Error("call CheckFundDetail response Status  =>" + response.StatusCode + " : Message : " + JsonConvert.SerializeObject(content));
                return null;
            }

        }
        catch (Exception ex)
        {
            _logger.Error("Error Exception CheckFundDetail =>" + ex.ToString());
            return null;
        }

    }
    public string Base64Encode(string plainText)
    {
        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
        return System.Convert.ToBase64String(plainTextBytes);
    }

    public string Base64Decode(string base64EncodedData)
    {
        var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
        return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
    }


    //--------------------------------------
    public Token GetAuthorizeToken(string baseAddress, string client_id, string client_secret, string username, string password)
    {


        try
        {

            _logger.Information("------call SF Token Start------");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var client = new RestClient(baseAddress);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            var request = new RestRequest(Method.POST);
            request.AddParameter("Content-Type", "application/x-www-form-urlencoded", ParameterType.HttpHeader);
            string encodedBody = string.Format("grant_type=password&client_id={0}&client_secret={1}&username={2}&password={3}", client_id, client_secret, username, password);
            request.AddParameter("application/x-www-form-urlencoded", encodedBody, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {

                Token result = JsonConvert.DeserializeObject<Token>(response.Content);
                _logger.Information("------call Token End------");
                return result;

            }
            else
            {
                _logger.Error("call Token response Status  =>" + response.StatusCode + " : Message : " + JsonConvert.SerializeObject(response.Content));
                return null;
            }

        }
        catch (Exception ex)
        {
            _logger.Error("Error Exception Token =>" + ex.ToString());
            return null;
        }
    }

    public SFQueryResult GetExistWelcomeCall(string tokenString, string instance_url, string SFQueryAPIURL, string SFQuerySOQLExistsWelcomeCall)
    {
        string APIEndPoint = instance_url + SFQueryAPIURL + SFQuerySOQLExistsWelcomeCall;

        try
        {

            _logger.Information("------call ExistWelcomeCall on SF QueryResult Start------");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var client = new RestClient(APIEndPoint);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            var request = new RestRequest(Method.GET);
            request.Parameters.Clear();
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", "Bearer " + tokenString);


            var response = client.Execute(request);
            var content = response.Content;


            if (response.StatusCode == HttpStatusCode.OK)
            {

                SFQueryResult result = JsonConvert.DeserializeObject<SFQueryResult>(content);
                _logger.Information("------call ExistWelcomeCall on SF QueryResult End------");
                return result;

            }
            else
            {
                _logger.Error("call ExistWelcomeCall on SF QueryResult response Status  =>" + response.StatusCode + " : Message : " + JsonConvert.SerializeObject(response.Content));
                return null;
            }

        }
        catch (Exception ex)
        {
            _logger.Error("Error Exception ExistWelcomeCall on SF QueryResult =>" + ex.ToString());
            return null;
        }
    }
}
