﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBuilder.Models;
public class RpqAssessment
{
    public int InvestorRiskLevel { get; set; }
    public string DescriptionTH { get; set; }
    public string DescriptionEN { get; set; }
    public string AllowedFundRiskLevel { get; set; }
    public string InvestorType { get; set; }
    public int Score { get; set; }
}
