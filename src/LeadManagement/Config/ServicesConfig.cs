﻿namespace LeadManagement.Config;

public class ServicesConfig
{
    public class SalesforceServiceConfig
    {
        public string LoginUrl { get; set; } = "";
        public string ClientId { get; set; } = "";
        public string ClientSecret { get; set; } = "";
        public string Username { get; set; } = "";
        public string Password { get; set; } = "";
    }

    public class UcrmServiceConfig
    {
        public string CreateWelcomeCallPath { get; set; } = "";
    }

    public class CcmServiceConfig
    {
        public string SmsUrl { get; set; } = "";
    }

    public SalesforceServiceConfig Salesforce { get; set; } = new SalesforceServiceConfig();
    public UcrmServiceConfig Ucrm { get; set; } = new UcrmServiceConfig();
    public CcmServiceConfig Ccm { get; set; } = new CcmServiceConfig();
}
