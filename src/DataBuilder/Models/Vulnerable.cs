﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBuilder.Models;
public class Vulnerable
{
    public string PolicyNo { get; set; }
    public string VulnerableType { get; set; }
    public string VulnerableFlag { get; set; }

}
