﻿using LeadManagement.Config;
using LeadManagement.Context;
using LeadManagement.Dtos.Ccm;
using LeadManagement.Dtos.Sms;
using LeadManagement.Extensions;
using LeadManagement.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Text;
using static LeadManagement.Config.ServicesConfig;

namespace LeadManagement.Services;

public class CcmService : ICcmService
{
    private readonly Serilog.ILogger _logger;
    private readonly ServicesConfig _servicesConfig;
    private readonly HttpClient _client;
    private readonly DataContext _dataContext;

    public CcmService(Serilog.ILogger logger, ServicesConfig servicesConfig, HttpClient client, DataContext dataContext)
    {
        _logger = logger;
        _servicesConfig = servicesConfig;
        _client = client;
        _dataContext = dataContext;
    }



    public async Task ResendSms()
    {
        CcmServiceConfig ccm = _servicesConfig.Ccm;
        var sentPolicyNos = _dataContext.WelcomeCallRequests.Select(r => r.PolicyNumber).ToList();
        var smses = _dataContext.ResendSmses.Where(r => !sentPolicyNos.Contains(r.PolicyNo));

        foreach (var sms in smses.ToList())
        {
            var json = JObject.Parse(sms.RequestPayload);
            json.Remove("SourceOfSubmission");
            json.Add("SourceOfSubmission", "LM");
            _logger.Information("JSON => {@json}", json);
            var payload = new StringContent(json.ToString(), Encoding.UTF8, "application/json");
            _logger.Information("Call to {Url} with {@Payload}", ccm.SmsUrl, payload);
            HttpResponseMessage res = await _client.PostAsync(ccm.SmsUrl, payload);
            string content = await res.Content.ReadAsStringAsync();
            var smsResponse = JsonConvert.DeserializeObject<CreateSmsResponseDto>(content);
            _logger.Information("Receive {@Response}", smsResponse);

            if (res.IsSuccessStatusCode)
            {
                sms.Status = "Sent";
            }
            else
            {
                sms.Status = "Fail";
                sms.StatusMessage = smsResponse?.Message ?? "";
            }

            _dataContext.Update(sms);
            await _dataContext.SaveChangesAsync();

            if (smsResponse == null)
            {
                throw new Exception("SMS response has no content.");
            }
        }

        _logger.Information("Change status of {@SentPolicyNos} records to UserReceived", sentPolicyNos.ToList());
        foreach (var policyNo in sentPolicyNos.ToList())
        {
            _dataContext.Database.ExecuteSqlRaw("UPDATE ResendSmses SET Status = 'UserReceived' WHERE PolicyNo = {0} AND Status = 'Pending'", policyNo);
        }
    }
}
