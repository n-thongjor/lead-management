﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBuilder.Queries;
internal class Dwh
{
    public static string QueryDWHSourceOfBiz() => @"
    select DESCITEM,SHORTDESC,LONGDESC,'LA' core from LA_DESCPF DESCPF where  DESCPF.DESCPFX='IT' and DESCPF.DESCTABL='T3615' 
    union
    select DESCITEM,SHORTDESC,LONGDESC,'IL' core from IL_DESCPF DESCPF where  DESCPF.DESCPFX='IT' and DESCPF.DESCTABL='T3615' order by core,descitem
    ";

    public static string QueryDWHContractType() => @"
    select DESCITEM,SHORTDESC,LONGDESC,'IL' core from il_descpf where desctabl = 'T5673' and DESCPFX='IT'
    union
    select DESCITEM,SHORTDESC,LONGDESC,'LA' core  from LA_descpf where desctabl = 'T5688' and DESCPFX='IT' order by DESCITEM
    ";

    public static string QueryDWHBillingChannel() => @"
    select DESCITEM,SHORTDESC,LONGDESC,'LA' core  from LA_DESCPF where DESCTABL = 'T3620' and DESCPFX='IT'
    union
    select DESCITEM,SHORTDESC,LONGDESC,'IL' core  from IL_DESCPF where DESCTABL = 'T3620'  and DESCPFX='IT' order by DESCITEM
    ";

    public static string QueryDWHVulnerable() => @"
    select PolicyNo,VulnerableType,VulnerableFlag from fn_I_GetValnerableByPolicyList(@PolicyList) where VulnerableFlag ='YES';
    ";

    public static string QueryDWHCustomerTier() => @"
    select CardId,
    CustomerTier,
    CustomerTierDescription,
    case when TierEffectiveDate is not null then substring(convert(varchar,TierEffectiveDate,127),1,10) else null end TierEffectiveDate from fn_I_GetCustomerTierByIDList(@CustomerIDList);
    ";
}
