﻿using Newtonsoft.Json;

namespace LeadManagement.Dtos.Salesforce;

public class LoginResponseDto
{
    [JsonProperty("access_token")]
    public string AccessToken { get; set; }

    [JsonProperty("instance_url")]
    public string InstanceUrl { get; set; }

    public LoginResponseDto(string accesstoken, string instanceUrl)
    {
        AccessToken = accesstoken;
        InstanceUrl = instanceUrl;
    }

    public void Deconstruct(out string accessToken, out string instanceUrl)
    {
        accessToken = AccessToken;
        instanceUrl = InstanceUrl;
    }
}
