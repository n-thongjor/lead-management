﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBuilder.Models;
public class FundDocument
{
    public string DocumentType { get; set; }
    public string DocumentName { get; set; }
    public string SourceName { get; set; }
    public string SourceUrl { get; set; }
}
