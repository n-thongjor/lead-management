﻿using LeadManagement.Dtos.Salesforce;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagement.Services;
public interface ISalesforceService
{
    public Task<LoginResponseDto> Login();
}
