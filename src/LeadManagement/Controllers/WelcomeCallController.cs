﻿using LeadManagement.Services;
using Microsoft.AspNetCore.Mvc;
using LeadManagement.Dtos.Ucrm;
using LeadManagement.Dtos.WelcomeCall;

namespace LeadManagement.Api.Controllers;

[Route("api/v{version:apiVersion}/[controller]")]
[ApiController]
[ApiVersion("1.0")]
public class WelcomeCallController : ControllerBase
{
    private readonly Serilog.ILogger _logger;
    private readonly IWelcomeCallService _welcomeCallService;

    public WelcomeCallController(Serilog.ILogger logger, IWelcomeCallService welcomeCallService)
    {
        _logger = logger;
        _welcomeCallService = welcomeCallService;
    }

    [HttpPost]
    public async Task<ActionResult<CreateWelcomeCallResponseDto>> CreateWelcomeCall([FromBody] CreateUcrmWelcomeCallFormDto form)
    {
        _logger.Debug("CreateWelcomeCall is called");
        try
        {
            CreateWelcomeCallResponseDto welcomCallStatus = await _welcomeCallService.CreateWelcomeCall(form);
            return CreatedAtAction(nameof(CreateWelcomeCall), welcomCallStatus);
        } catch(Exception ex)
        {
            return BadRequest(new 
            {
                Status = "Fail",
                Message = ex.Message ?? "Something went wrong",
            });
        }
    }
}
