﻿using DataBuilder;
using LeadManagement.Config;
using LeadManagement.Context;
using LeadManagement.Dtos.Salesforce;
using LeadManagement.Dtos.Ucrm;
using LeadManagement.Extensions;
using LeadManagement.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagement.Services;
public class UcrmService : IUcrmService
{
    private readonly Serilog.ILogger _logger;
    private readonly ServicesConfig _servicesConfig;
    private readonly HttpClient _client;
    private readonly ISalesforceService _salesforce;
    private readonly IBuilder _dataBuilder;
    private readonly DataContext _dataContext;

    public UcrmService(Serilog.ILogger logger, ServicesConfig servicesConfig, HttpClient client, ISalesforceService salesforce, IBuilder dataBuilder, DataContext dataContext)
    {
        _logger = logger;
        _servicesConfig = servicesConfig;
        _client = client;
        _salesforce = salesforce;
        _dataBuilder = dataBuilder;
        _dataContext = dataContext;
    }

    public async Task<CreateUcrmWelcomeCallResponseDto> CreateWelcomeCall(CreateUcrmWelcomeCallFormDto form)
    {
        (string accessToken, string instanceUrl) = await _salesforce.Login();
        var url = $"{instanceUrl}{_servicesConfig.Ucrm.CreateWelcomeCallPath}";
        (string? payload, Exception? error, string? duplicateId) = _dataBuilder.Build(form.FirstIssueDate, form.PolicyNumber);

        if (error != null)
        {
            _logger.Error("Build data with {@Form} has thrown {Error}", form, error);
            throw error;
        }

        if(!string.IsNullOrEmpty(duplicateId))
        {
            var duplicateLead = new DuplicateLead
            {
                DuplicateId = duplicateId,
                PolicyNo = form.PolicyNumber
            };
            _dataContext.Add(duplicateLead);
            await _dataContext.SaveChangesAsync();
            _logger.Error("Duplicate {Id} ID was found", duplicateId);
            throw new Exception("Duplicate Id : " + duplicateId);
        }

        if (string.IsNullOrEmpty(payload))
        {
            _logger.Error("{@Payload} is empty or null", payload);
            throw new Exception("Payload is empty or null"); ;
        }

        _client.DefaultRequestHeaders.Add("Authorization", $"Bearer {accessToken}");
        _logger.Information("Call {Url} with {@Payload}", url, payload);
        HttpResponseMessage res = await _client.PostAsync(url, new StringContent(payload, Encoding.UTF8, "application/json"));
        string content = await res.Content.ReadAsStringAsync();
        var welcomeCallResponse = JsonConvert.DeserializeObject<CreateUcrmWelcomeCallResponseDto>(content);
        _logger.Information("Receive {@Response} from {Url}", welcomeCallResponse, url);

        if (welcomeCallResponse == null)
        {
            throw new Exception("Create welcome call response has no content.");
        }

        return welcomeCallResponse;
    }
}
