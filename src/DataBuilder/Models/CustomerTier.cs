﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBuilder.Models;
public class CustomerTier
{
    public string CardId { get; set; }
    public string CustomerTierLevel { get; set; }
    public string CustomerTierDescription { get; set; }
    public string TierEffectiveDate { get; set; }

}
