﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace LeadManagement.Migrations
{
    public partial class AddProcessedToWelcomeCallRequestTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Processed",
                table: "WelcomeCallRequests",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Processed",
                table: "WelcomeCallRequests");
        }
    }
}
