﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBuilder.Queries;
internal class Sf
{
    public static string SFQuerySOQLExistsWelcomeCall() => @"
    SELECT Policy_Number__c,Id 
    FROM Welcome_Call__c 
    WHERE First_Issue_Date__c = LAST_N_DAYS:60
    ORDER BY Id DESC
    ";
}
