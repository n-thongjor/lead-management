﻿using LeadManagement.Context;
using LeadManagement.Dtos.Ucrm;
using LeadManagement.Dtos.WelcomeCall;
using LeadManagement.Models;

namespace LeadManagement.Services;
public class WelcomeCallService : IWelcomeCallService
{
    private readonly Serilog.ILogger _logger;
    private readonly IUcrmService _ucrmService;
    private readonly DataContext _dataContext;

    public WelcomeCallService(Serilog.ILogger logger, IUcrmService ucrmService, DataContext dataContext)
    {
        _logger = logger;
        _ucrmService = ucrmService;
        _dataContext = dataContext;
    }

    public async Task<CreateWelcomeCallResponseDto> CreateWelcomeCall(CreateUcrmWelcomeCallFormDto form)
    {
        _logger.Information("Create welcome call process starts with {@Input}", form);
        CreateUcrmWelcomeCallResponseDto res = await _ucrmService.CreateWelcomeCall(form);

        var welcomeCallRequest = new WelcomeCallRequest
        {
            PolicyNumber = form.PolicyNumber,
            FirstIssueDate = form.FirstIssueDate
        };
        _dataContext.Add(welcomeCallRequest);
        await _dataContext.SaveChangesAsync();


        return new CreateWelcomeCallResponseDto
        {
            Id = res.Id,
            Status = res.Success ? "success" : "fail",
            Message = String.Join(",", res.Errors),
        };
    }
}
