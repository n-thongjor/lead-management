﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBuilder.Queries;
internal class Mis
{
    public static string QueryMISSalesStruct() => @"
    SELECT case 
    when substring(column1,1,1)='S' then '61'+replace(column1,'S','') 
    when substring(column1,1,1)='T' then '61'+replace(column1,'T','') 
    when substring(column1,1,1)='A' then '51'+replace(column1,'A','') 
    when substring(column1,1,1)='M' then '66'+replace(column1,'M','') 
    when substring(column1,1,1)='R' then '68'+replace(column1,'R','') 
    when substring(column1,1,1)='C' then '52'+replace(column1,'C','') 
    when substring(column1,1,1)='K' then '66'+replace(column1,'K','') 
    when substring(column1,1,1)='G' then '67'+replace(column1,'G','') 
    when substring(column1,1,1)='E' then '53'+replace(column1,'E','') 
    when substring(column1,1,2)='HH' then '651'+replace(column1,'H','') 
    when substring(column1,1,2)='XX' then '652'+replace(column1,'X','') 
    else column1 end fwdcolumn1,
    column1,
    case 
    when substring(column2,1,1)='S' then '61'+replace(column2,'S','') 
    when substring(column2,1,1)='T' then '61'+replace(column2,'T','') 
    when substring(column2,1,1)='A' then '51'+replace(column2,'A','') 
    when substring(column2,1,1)='M' then '66'+replace(column2,'M','') 
    when substring(column2,1,1)='R' then '68'+replace(column2,'R','') 
    when substring(column2,1,1)='C' then '52'+replace(column2,'C','') 
    when substring(column2,1,1)='K' then '66'+replace(column2,'K','') 
    when substring(column2,1,1)='G' then '67'+replace(column2,'G','') 
    when substring(column2,1,1)='E' then '53'+replace(column2,'E','') 
    when substring(column2,1,2)='HH' then '651'+replace(column2,'H','') 
    when substring(column2,1,2)='XX' then '652'+replace(column2,'X','') 
    else column2 end fwdcolumn2,
    column2,column3 FROM [MISDB].[dbo].[misAPE_Upload] where [ConfigurationType]='SCB_Alternative_Sales'
    ";

    public static string QueryMISSalesSegment() => @"
    select Column1 staffid,Column2 scbstaffid,Column46 segment ,Column47 segmentGroup ,Column49 segmentAPE 
    from [dbo].[misAPE_Upload] where ConfigurationType='UploadSaleStructure'
    ";
}
