﻿namespace DataBuilder.Config;

public class DataBuilderConfig
{
    public class ConnectionStringsConfig
    {
        public string ODS { get; set; }
        public string DWH { get; set; }
        public string MIS { get; set; }
        public string Eform { get; set; }
    }

    public ConnectionStringsConfig ConnectionStrings { get; set; }
    public int DBConnectionTimeOut { get; set; }
    public int WaitingTimePerRequest { get; set; }
    public string ServerFundWS { get; set; }
    public string MethodRpqassessment { get; set; }
    public string MethodFunddetail { get; set; }
    public string SFbaseAddress { get; set; }
    public string SFclientId { get; set; }
    public string SFclientSecret { get; set; }
    public string SFusername { get; set; }
    public string SFpassword { get; set; }
    public string SFQueryAPIURL { get; set; }
}
