﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBuilder.Models;
public class SFQueryRecords
{
    public SFQueryRecordsAttributes attributes { get; set; }
    public string Policy_Number__c { get; set; }
    public string Id { get; set; }
}
