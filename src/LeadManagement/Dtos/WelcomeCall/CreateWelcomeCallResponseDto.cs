﻿namespace LeadManagement.Dtos.WelcomeCall;

public class CreateWelcomeCallResponseDto
{
    public string Id { get; set; } = "";
    public string Status { get; set; } = "fail";
    public string Message { get; set; } = "";

    public CreateWelcomeCallResponseDto()
    {

    }

    public CreateWelcomeCallResponseDto(string id, string status, string message)
    {
        Id = id;
        Status = status;
        Message = message;
    }
}
