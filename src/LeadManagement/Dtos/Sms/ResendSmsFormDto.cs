﻿using Newtonsoft.Json;
using System.Text.Json;

namespace LeadManagement.Dtos.Sms;
public record ResendSmsFormDto
{
    public record SmsCommunication
    {
        [JsonProperty("preferLang")]
        public string? PreferLang { get; set; }

        [JsonProperty("mobileNoList")]
        public string[]? MobileNoList { get; set; }

    }

    public record SmsParameters
    {
        [JsonProperty("policyNo")]
        public string? PolicyNo { get; set; }

        [JsonProperty("sourceOfBusiness")]
        public string? SourceOfBusiness { get; set; }

        [JsonProperty("basePlanCode")]
        public string? BasePlanCode { get; set; }

        [JsonProperty("basePlanDescription")]
        public string? BasePlanDescription { get; set; }

        [JsonProperty("contractType")]
        public string? ContractType { get; set; }

        [JsonProperty("productType")]
        public string? ProductType { get; set; }

        [JsonProperty("policyDeliveryChannel")]
        public string? PolicyDeliveryChannel { get; set; }

        [JsonProperty("effectiveDate")]
        public string? EffectiveDate { get; set; }

        [JsonProperty("insuredFirstName")]
        public string? InsuredFirstName { get; set; }

        [JsonProperty("insuredLastName")]
        public string? InsuredLastName { get; set; }

        [JsonProperty("titleName")]
        public string? TitleName { get; set; }

        [JsonProperty("ownerFirstName")]
        public string? OwnerFirstName { get; set; }

        [JsonProperty("ownerLastName")]
        public string? OwnerLastName { get; set; }

        [JsonProperty("applicationNo")]
        public string? ApplicationNo { get; set; }

        [JsonProperty("issueDate")]
        public string? IssueDate { get; set; }

        [JsonProperty("systemName")]
        public string? SystemName { get; set; }

        [JsonProperty("customerName")]
        public string? CustomerName { get; set; }

        [JsonProperty("groupCode")]
        public string? GroupCode { get; set; }

        [JsonProperty("ownerCardId")]
        public string? OwnerCardId { get; set; }

        [JsonProperty("insureCardId")]
        public string? InsureCardId { get; set; }

        [JsonProperty("ownerOccupationCode")]
        public string? OwnerOccupationCode { get; set; }

        [JsonProperty("ownerOccupationDescription")]
        public string? OwnerOccupationDescription { get; set; }

        [JsonProperty("insureOccupationCode")]
        public string? InsureOccupationCode { get; set; }

        [JsonProperty("insureOccupationDescription")]
        public string? InsureOccupationDescription { get; set; }

        [JsonProperty("issueAge")]
        public string? IssueAge { get; set; }

        [JsonProperty("firstTotalPremium")]
        public string? FirstTotalPremium { get; set; }

        [JsonProperty("submissionDate")]
        public string? SubmissionDate { get; set; }
    }

    [JsonProperty("sourceOfSubmission")]
    public string? SourceOfSubmission { get; set; }

    [JsonProperty("reportType")]
    public string? ReportType { get; set; }

    [JsonProperty("reportKey")]
    public string? ReportKey { get; set; }

    [JsonProperty("communication")]
    public SmsCommunication? Communication { get; set; }

    [JsonProperty("isNewReport")]
    public bool IsNewReport { get; set; }

    [JsonProperty("returnBinary")]
    public bool ReturnBinary { get; set;  }

    [JsonProperty("parameters")]
    public SmsParameters? Parameters { get; set;  }
  
}
