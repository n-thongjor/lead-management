﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBuilder.Models;
public class SourceOfBiz
{
    public string SHORTDESC { get; set; }
    public string LONGDESC { get; set; }
    public string DESCITEM { get; set; }
    public string CORE { get; set; }
}
