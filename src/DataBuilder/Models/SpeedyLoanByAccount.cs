﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBuilder.Models;
public class SpeedyLoanByAccount
{
    public string InsuredCitizenNo { get; set; }
    public string AppLoanNumber { get; set; }
    public string AccountNumber { get; set; }
    public string ChannelPackageCode { get; set; }

}
