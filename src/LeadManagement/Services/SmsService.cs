﻿using LeadManagement.Context;
using LeadManagement.Dtos.Ccm;
using LeadManagement.Dtos.Sms;
using LeadManagement.Extensions;
using LeadManagement.Models;
using System.Net;

namespace LeadManagement.Services;
public class SmsService : ISmsService
{
    private readonly Serilog.ILogger _logger;
    private readonly ICcmService _ccmService;
    private readonly DataContext _dataContext;

    public SmsService(Serilog.ILogger logger, ICcmService ccmService, DataContext dataContext)
    {
        _logger = logger;
        _ccmService = ccmService;
        _dataContext = dataContext;
    }

    public async Task<string> EnqueueSms(ResendSmsFormDto form)
    {
        var sendReportKey = Guid.NewGuid().ToString();
        var sms = new ResendSms
        {
            SourceOfSubmission = "LM",
            ReportType = form.ReportType ?? "",
            ReportKey = form.ReportKey ?? "",
            SendReportKey = sendReportKey,
            PolicyNo = form.Parameters?.PolicyNo ?? "",
            SubmissionDate = form.Parameters?.SubmissionDate ?? "",
            RequestPayload = await form.AsJson().ReadAsStringAsync() ?? ""
        };
        _dataContext.Add(sms);
        await _dataContext.SaveChangesAsync();

        return sendReportKey;
    }
}
