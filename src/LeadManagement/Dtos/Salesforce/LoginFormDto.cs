﻿using System.ComponentModel.DataAnnotations;

namespace LeadManagement.Dtos.Salesforce;
internal class LoginFormDto
{
    [Required]
    public string ClientId { get; set; }

    [Required]
    public string ClientSecret { get; set; }

    [Required]
    public string Username { get; set; }

    [Required]
    public string Password { get; set; }

    public LoginFormDto(string clientId, string clientSecret, string username, string password)
    {
        ClientId = clientId;
        ClientSecret = clientSecret;
        Username = username;
        Password = password;
    }
}
