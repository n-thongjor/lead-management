﻿using LeadManagement.Dtos;
using LeadManagement.Dtos.Ucrm;
using LeadManagement.Dtos.WelcomeCall;

namespace LeadManagement.Services;
public interface IWelcomeCallService
{
    public Task<CreateWelcomeCallResponseDto> CreateWelcomeCall(CreateUcrmWelcomeCallFormDto form);
}
