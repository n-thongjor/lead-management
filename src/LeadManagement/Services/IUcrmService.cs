﻿using LeadManagement.Dtos.Ucrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadManagement.Services;
public interface IUcrmService
{
    public Task<CreateUcrmWelcomeCallResponseDto> CreateWelcomeCall(CreateUcrmWelcomeCallFormDto form);
}
