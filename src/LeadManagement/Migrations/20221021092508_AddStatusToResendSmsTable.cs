﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace LeadManagement.Migrations
{
    public partial class AddStatusToResendSmsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "ResendSmses",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "StatusMessage",
                table: "ResendSmses",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "ResendSmses");

            migrationBuilder.DropColumn(
                name: "StatusMessage",
                table: "ResendSmses");
        }
    }
}
