﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBuilder.Queries;
internal class FwdLink
{
    public static string QueryFwdLinkAcceptCustomer() => @"
    SELECT policyno,actioncode,senddate FROM customerportal$welcomesms a  WHERE actioncode='Accept' AND senddate>= DATE_ADD( NOW(),INTERVAL -31 DAY)
    ";
}
