﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DataBuilder.Builder;

namespace DataBuilder;
public interface IBuilder
{
    public BuildResponse Build(string firstIssueDate, string policyNo);
}
