﻿using LeadManagement.Models;
using Microsoft.EntityFrameworkCore;

namespace LeadManagement.Context;

public class DataContext : DbContext
{
    public DataContext(DbContextOptions options) : base(options)
    {

    }

    public DbSet<ResendSms> ResendSmses { get; set; }
    public DbSet<WelcomeCallRequest> WelcomeCallRequests { get; set; }
    public DbSet<DuplicateLead> DuplicateLeads { get; set; }

    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
    {
        var insertedEntries = this.ChangeTracker.Entries()
                               .Where(x => x.State == EntityState.Added)
                               .Select(x => x.Entity);

        foreach (var insertedEntry in insertedEntries)
        {
            if (insertedEntry is BaseModel auditableEntity)
            {
                auditableEntity.CreatedOn = DateTimeOffset.UtcNow;
            }
        }

        var modifiedEntries = this.ChangeTracker.Entries()
                   .Where(x => x.State == EntityState.Modified)
                   .Select(x => x.Entity);

        foreach (var modifiedEntry in modifiedEntries)
        {
            if (modifiedEntry is BaseModel auditableEntity)
            {
                auditableEntity.UpdatedOn = DateTimeOffset.UtcNow;
            }
        }

        return base.SaveChangesAsync(cancellationToken);
    }
}
