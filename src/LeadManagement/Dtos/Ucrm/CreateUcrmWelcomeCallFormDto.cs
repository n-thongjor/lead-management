﻿using System.ComponentModel.DataAnnotations;

namespace LeadManagement.Dtos.Ucrm;
public class CreateUcrmWelcomeCallFormDto
{
    [Required]
    public string PolicyNumber { get; set; }

    [Required]
    [RegularExpression(@"\d{4}-\d{2}-\d{2}")]
    public string FirstIssueDate { get; set; }

    public CreateUcrmWelcomeCallFormDto(string policyNumber, string firstIssueDate)
    {
        PolicyNumber = policyNumber;
        FirstIssueDate = firstIssueDate;
    }
}
