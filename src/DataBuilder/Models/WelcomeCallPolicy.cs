﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBuilder.Models;
public class WelcomeCallPolicy
{
    public string PolicyNo { get; set; }
    public DateTime ProposalDate { get; set; }
    public DateTime ProposalReceivedDate { get; set; }
    public DateTime EffectiveDate { get; set; }
    public DateTime FirstIssueDate { get; set; }
    public DateTime IssueDate { get; set; }
    public string SourceofBusiness { get; set; }
    public string ContractType { get; set; }
    public string PlanCode { get; set; }
    public string InternalPlanCode { get; set; }
    public string PlanDescription { get; set; }
    public string CoveragePeriod { get; set; }
    public string PremiumPaymentPeriod { get; set; }
    public string BillingFrequency { get; set; }
    public string BillingChannel { get; set; }
    public string PolicyDeliveryMode { get; set; }
    public string ModalPremium { get; set; }
    public string InsuredIDNo { get; set; }
    public string InsuredTitleName { get; set; }
    public string InsuredFirstName { get; set; }
    public string InsuredLastName { get; set; }
    public string InsuredMobileNo { get; set; }
    public string InsuredHomePhone { get; set; }
    public string InsuredEMailAddress { get; set; }
    public string DespatchAddrLine1 { get; set; }
    public string DespatchAddrLine2 { get; set; }
    public string DespatchAddrLine3 { get; set; }
    public string DespatchAddrLine4 { get; set; }
    public string DespatchAddrLine5 { get; set; }
    public string DespatchProvince { get; set; }
    public string DespatchPostcode { get; set; }
    public string OwnerFirstName { get; set; }
    public string OwnerLastName { get; set; }
    public string OwnerMobileNo { get; set; }
    public string OwnerEMailAddress { get; set; }
    public string PayorFirstName { get; set; }
    public string PayorLastName { get; set; }
    public string AgentNo { get; set; }
    public string AgentFirstName { get; set; }
    public string AgentLastName { get; set; }
    public string AgentMobileNo { get; set; }
    public string AgentBranchCode { get; set; }
    public string InternalFundCode { get; set; }
    public string RiskProfileScore { get; set; }
    public string VulnerableFlag { get; set; }
    public string STFundShortDesc { get; set; }
    public string Memo { get; set; }
    public string ICPOption { get; set; }
    public string ICPPaymentMethod { get; set; }
    public string SHORTDESC { get; set; }
    public string LONGDESC { get; set; }
    public string InsuredAge { get; set; }
}
