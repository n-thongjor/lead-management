﻿using System.ComponentModel.DataAnnotations;

namespace LeadManagement.Models;

public class DuplicateLead : BaseModel
{
    public int Id { get; set; }

    [Required]
    public string DuplicateId { get; set; } = "";

    [Required]
    public string PolicyNo { get; set; } = "";
}
