﻿using Newtonsoft.Json;
using System.Text;

namespace LeadManagement.Extensions;
internal static class ObjectExtension
{
    public static StringContent AsJson(this object o)
        => new(JsonConvert.SerializeObject(o), Encoding.UTF8, "application/json");

}
