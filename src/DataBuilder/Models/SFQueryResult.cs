﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBuilder.Models;
public class SFQueryResult
{
    public string totalSize { get; set; }
    public string done { get; set; }
    public string nextRecordsUrl { get; set; }
    public SFQueryRecords[] records { get; set; }
}
