﻿namespace LeadManagement.Dtos.Ucrm;

public class CreateUcrmWelcomeCallResponseDto
{
    public string Id { get; set; }
    public bool Success { get; set; }
    public string[] Errors { get; set; }

    public CreateUcrmWelcomeCallResponseDto(string id, bool success, string[] errors)
    {
        Id = id;
        Success = success;
        Errors = errors;
    }
}
