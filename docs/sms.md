# SMS Management

![Lead Management](./images/LeadManagement.png)

## Enqueue SMS

> Endpoint: /api/v1/sms/enqueue
>
> HTTP Method: POST
>
> Flow: 2

Input:

```json
{
	"sourceOfSubmission": "NBA",
	"reportType": "WELCOME_NEW_POLICY",
	"reportKey": "o69650715711666164345966",
	"communication": {
		"preferLang": "TH",
		"mobileNoList": [
			"0825541569"
		]
	},
	"isNewReport": true,
	"returnBinary": false,
	"parameters": {
		"policyNo": "40784000",
		"sourceOfBusiness": "BB",
		"basePlanCode": "BE0H",
		"basePlanDescription": "18/7 Return Plus (���Թ�ѹ��)",
		"contractType": "E38",
		"productType": "IL",
		"policyDeliveryChannel": "EMAL",
		"effectiveDate": "2022-10-19 00:00:00",
		"insuredFirstName": "��¹��17341",
		"insuredLastName": "���Թ",
		"titleName": "�ҧ���",
		"ownerFirstName": "��¹��17341",
		"ownerLastName": "���Թ",
		"applicationNo": "6965071571",
		"issueDate": "2022-10-19 00:00:00",
		"systemName": "IL",
		"customerName": "��¹��17341 ���Թ",
		"groupCode": "",
		"ownerCardID": "3417142318547",
		"insureCardID": "3417142318547",
		"ownerOccupationCode": "1161",
		"ownerOccupationDescription": "��������-��ӹѡ�ҹ",
		"insureOccupationCode": "1161",
		"insureOccupationDescription": "��������-��ӹѡ�ҹ",
		"issueAge": "35",
		"firstTotalPremium": "250000.0",
		"submissionDate": "2022-10-17 14:25:55"
	}
}
```

CURL

```
curl -X 'POST' \
  'https://localhost:7248/api/v1/Sms' \
  -H 'accept: text/plain' \
  -H 'Content-Type: application/json' \
  -d '{
	"sourceOfSubmission": "LM",
	"reportType": "WELCOME_NEW_POLICY",
	"reportKey": "o69650715711666164345966",
	"communication": {
		"preferLang": "TH",
		"mobileNoList": [
			"0825541569"
		]
	},
	"isNewReport": true,
	"returnBinary": false,
	"parameters": {
		"policyNo": "40784000",
		"sourceOfBusiness": "BB",
		"basePlanCode": "BE0H",
		"basePlanDescription": "18/7 Return Plus (���Թ�ѹ��)",
		"contractType": "E38",
		"productType": "IL",
		"policyDeliveryChannel": "EMAL",
		"effectiveDate": "2022-10-19 00:00:00",
		"insuredFirstName": "��¹��17341",
		"insuredLastName": "���Թ",
		"titleName": "�ҧ���",
		"ownerFirstName": "��¹��17341",
		"ownerLastName": "���Թ",
		"applicationNo": "6965071571",
		"issueDate": "2022-10-19 00:00:00",
		"systemName": "IL",
		"customerName": "��¹��17341 ���Թ",
		"groupCode": "",
		"ownerCardID": "3417142318547",
		"insureCardID": "3417142318547",
		"ownerOccupationCode": "1161",
		"ownerOccupationDescription": "��������-��ӹѡ�ҹ",
		"insureOccupationCode": "1161",
		"insureOccupationDescription": "��������-��ӹѡ�ҹ",
		"issueAge": "35",
		"firstTotalPremium": "250000.0",
		"submissionDate": "2022-10-17 14:25:55"
	}
}'
```

Success Response:

HTTP Status: 201 Created

```json
{
    "status": "SUCCESS",
    "message": "SUCCESS",
    "data": {
        "rptId": "47242"
    }
}
```

Error Response:

HTTP Status: 400 Bad Request, 500 Internal Server Error

```json
{
    "status": "GENERAL_SERVER_ERROR",
    "message": "DataIntegrityViolationException:Cannot insert the value NULL into column 'source_of_submission', table 'ccm_db.dbo.API_CCM_RPT_INFO'; column does not allow nulls. INSERT fails."
}
```

## Resend SMS

> Endpoint: /api/v1/sms
>
> HTTP Method: POST
>
> Flow: 11

Success Response:

HTTP Status: 201 Created

```json
{
    "status": "SUCCESS",
    "message": "SUCCESS",
}
```

Error Response:

HTTP Status: 400 Bad Request, 500 Internal Server Error

```json
{
    "status": "GENERAL_SERVER_ERROR",
    "message": "DataIntegrityViolationException:Cannot insert the value NULL into column 'source_of_submission', table 'ccm_db.dbo.API_CCM_RPT_INFO'; column does not allow nulls. INSERT fails."
}
```